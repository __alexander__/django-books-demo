# Django books demo

Proyecto en django que muestra un ejemplo de cómo implementar sitemaps para una página que gestiona libros que tiene más de un millón de registros

# Carga inicial de datos

1. Ejecutar las migraciones para que se creen las tablas necesarias (books)

2. Descargar y descomprimir el zip de libros desde:

    <a href="https://gitlab.com/__alexander__/one-million-books" target="_blank">One million books</a>

3. Colocar en archivo en la carpeta del proyecto en django y cargarlo mediante el comando:


    python manage.py shell < scripts/load_books.py

Esto buscará el archivo mybooks.json y lo cargará en django. Y esto último lo podemos validar en el administrador del mismo (/admin).