from django.db import models
from django.shortcuts import reverse


class Author(models.Model):
    name = models.CharField(max_length=128)

    def __str__(self):
        return self.name


class Book(models.Model):
    title = models.CharField(max_length=256)
    author = models.ForeignKey(Author, on_delete=models.SET_NULL, null=True)
    genre = models.CharField(max_length=128)
    publish_date = models.CharField(max_length=10)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('books:book_detail', kwargs={'id': self.id})
