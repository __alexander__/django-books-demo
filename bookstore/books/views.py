from .models import Book
from django.http import HttpResponse


def book_detail(request, id):
    book = Book.objects.get(id=id)
    return HttpResponse(book.title)
