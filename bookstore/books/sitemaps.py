from django.contrib.sitemaps import Sitemap
from .models import Book


class BooksSitemap(Sitemap):
    changefreq = 'weekly'
    priority = 0.5
    protocol = 'http'

    def items(self):
        return Book.objects.all().order_by('-id')
