from django.contrib.sitemaps.views import sitemap
from django.urls import path

from .sitemaps import BooksSitemap
from . import views

sitemaps = {
    'books': BooksSitemap,
}

app_name = 'books'

urlpatterns = [
    path("id/<int:id>/", views.book_detail, name='book_detail'),
    path('sitemap.xml', sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap')
]
