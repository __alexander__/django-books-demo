from books.models import Author, Book
import json


def load_books():
    filename = 'mybooks.json'
    with open(filename) as file:
        _books = json.load(file)
        for _book in _books:
            author, _ = Author.objects.get_or_create(name=_book.get('author', {}).get('name'))
            Book.objects.create(
                title=_book.get('name'),
                genre=_book.get('genre'),
                publish_date=_book.get('publish_date'),
                author=author
            )


load_books()
